with pkg_sem; use pkg_sem; 

package pkg_tarea is 
   
   task type tarea_suceso_t(sem : pkg_sem.p_sem);
	type tarea_p is access tarea_suceso_t;   
   
end pkg_tarea;
