with Ada.Real_Time; use Ada.Real_Time;

with pkg_sem; use pkg_sem;
with pkg_tarea; use pkg_tarea;

procedure main is
   Semaforo: p_sem := new sem_t;
   Tarea: tarea_p := new tarea_suceso_t(Semaforo);
begin
   for i in 0..10 loop
      delay duration(i);
      signal(Semaforo.all);
   end loop;
   abort Tarea.all;
end main;
