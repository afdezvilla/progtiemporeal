with Ada.Text_Io; use Ada.Text_Io;
with Ada.Task_Identification; use Ada.Task_Identification;

with pkg_sem; use pkg_sem;
with pkg_funct_time; use pkg_funct_time;

package body pkg_tarea is
   Contador : Integer := 0;
   Id : Task_Id;
   task body tarea_suceso_t is
   begin
      Id := Current_Task;
      loop
         select
            delay(0.01);
            Contador := Contador + 1;
            wait(sem.all);
            Put_Line("Evento generado a las :" &time_to_string);
            Put_Line("Valor del contador"&Integer'Image(Contador));
            Contador := 0;  
         then abort
            loop
               delay(0.1);
            end loop;
         end select;
         if not Is_Callable(Id) then
            exit;
         end if;
      end loop;
   end tarea_suceso_t;
end Pkg_Tarea;
