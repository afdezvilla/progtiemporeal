with Text_IO; use Text_IO;

package body pkg_tarea is
   task body tarea_t is
      ContadorPar : Integer := 0;
      ContadorImpar : Integer := 0;
   begin
      loop
         select
            accept EsPar(N : integer) do
               if N rem 2=0 then
                  Put_Line("El n�mero es "& Integer'Image(N)&" Par");
                  ContadorPar := ContadorPar+1;
               else
                  Put_Line("El n�mero es "& Integer'Image(N)&" Impar");
                  ContadorImpar := ContadorImpar+1;
                  end if;
            end EsPar;
         or
            accept EstadoConsultas do
               Put_Line("El n�mero de n�meros pares consultados es "& Integer'Image(ContadorPar));
               Put_Line("El n�mero de n�meros impares consultados es "& Integer'Image(ContadorImpar));
            end EstadoConsultas;
         end select;
      end loop;
   end tarea_t;
end pkg_tarea;
