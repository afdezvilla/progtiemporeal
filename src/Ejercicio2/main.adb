with Ada.Text_IO; use Ada.Text_IO;

with pkg_procedure; use pkg_procedure;
with pkg_tarea; use pkg_tarea;

procedure main is
   Tarea : tarea_t;
   Numero : Integer;
begin
   loop
      Tarea.EstadoConsultas;
      Leer_Entero(Numero);
      if Numero /= 0 then
         Tarea.EsPar(Numero);
      else
          exit;
      end if;
   end loop;
   abort Tarea;
end main;
