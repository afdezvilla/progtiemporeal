with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Task_Identification; use Ada.Task_Identification;
with pkg_monitor; use pkg_monitor;
procedure main is
	monitor : monitor_t;
	msg_aux : Unbounded_String;
	Task type writer_t;
	Task type reader_t;
	Task body writer_t is
	begin
		for I in Integer range 1 .. 5 loop
			delay 0.1;
				imprimir_msg(monitor, "Soy la tarea: "&Image(Current_Task));
			delay 3.0;
		end loop;
	end writer_t;
	Task body reader_t is
	begin
		for I in Integer range 1 .. 5 loop
			delay 3.0;
			get_msg(monitor, msg_aux);
			while msg_aux = "1" loop
				get_msg(monitor, msg_aux);
			end loop;
			imprimir_msg(monitor, "[" & Image(Current_Task) & "]: "& To_String(msg_aux));
		end loop;
	end reader_t;
	w1 : writer_t;
	w2 : writer_t;
	r1 : reader_t;
	r2 : reader_t;
begin
	imprimir_msg(monitor, "Inicio del programa main ");
	delay 60.0;
	abort w1;
	abort w2;
	abort r1;
	abort r2;
	imprimir_msg(monitor, "Fin del programa main ");
end main;