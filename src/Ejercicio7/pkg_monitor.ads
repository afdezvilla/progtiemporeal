with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;
package pkg_monitor is
   type monitor_t is limited private;
   procedure imprimir_msg(monitor : in out monitor_t; msg : in String);
   procedure get_msg(monitor      : in out monitor_t;
                     msg          : out Unbounded_String);
private
   type Status is (LIBRE, OCUPADO);
   protected type monitor_t is
      entry imprimir_msg(msg      : in String);
      procedure imprimir_aux(msg  : in String);
      entry bloquear;
      procedure liberar;
   private
      estado                      : Status := LIBRE;
   end monitor_t;
end pkg_monitor;
