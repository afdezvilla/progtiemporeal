with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO.Unbounded_IO; use Ada.Text_IO.Unbounded_IO;

package body pkg_monitor is 
   procedure imprimir_msg(monitor : in out monitor_t; msg : in String) is
   begin
      monitor.bloquear;
      monitor.imprimir_msg(msg);
      delay 2.0;
      monitor.liberar;
   end imprimir_msg;
   procedure get_msg(monitor : in out monitor_t; msg : out Unbounded_String) is
   begin
   		monitor.bloquear;
   		monitor.imprimir_aux("...Esperando a que escriba algo:");
   		select
            delay(10.0);
            msg:=To_Unbounded_String("-1");
            monitor.imprimir_aux("ERROR, Usuario despistado...");	
   		then abort
            msg:=To_Unbounded_String(Get_Line);
   		end select;
   		monitor.liberar;
   end get_msg;
protected body  monitor_t is 
      entry imprimir_msg(msg : in String) when estado = OCUPADO is
         begin
         Put_Line(To_Unbounded_String(msg));
       end imprimir_msg;   
      procedure imprimir_aux(msg  : in String)  is
      begin
         estado := OCUPADO;
      		Put_Line(To_Unbounded_String(msg));
      		estado := LIBRE;
      end imprimir_aux;
      entry bloquear when estado = LIBRE is
      begin
         estado := OCUPADO;
      end bloquear;
      procedure liberar is
      begin
         estado := LIBRE;
      end liberar;
end monitor_t;
end pkg_monitor;
