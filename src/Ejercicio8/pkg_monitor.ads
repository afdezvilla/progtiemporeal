with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package  pkg_monitor is

	type Status is (LIBRE, OCUPADO);
	protected type monitor_t is
		entry imprimir_msg(msg : in String);
		entry imprimir_aux(msg : in String);
		entry bloquear;
		procedure liberar;

	private
		estado : Status := LIBRE;
		estado_get : Status := LIBRE;
	end monitor_t;

	procedure get_msg(monitor : in out monitor_t; msg_in : in String; msg_out : out Unbounded_String);

	type p_monitor is access monitor_t;

end pkg_monitor;
