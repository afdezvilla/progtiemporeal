with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with pkg_monitor; use pkg_monitor;
with pkg_sem; use pkg_sem;
with pkg_tarea; use pkg_tarea;
with Ada.Task_Identification; use Ada.Task_Identification;

procedure main is
	monitor : p_monitor := new monitor_t;
	sem : p_sem := new sem_t;
	tarea1 : tarea_periodica_t(monitor, sem, 2000);
	tarea2 : tarea_periodica_t(monitor, sem, 5000);
	tarea_e : tarea_eventos_t(monitor, sem);
	tarea_c : tarea_codigo_t(monitor);

begin
	monitor.all.imprimir_msg("Inicio del programa main ");
	delay 1.0;
	while (Is_Callable(tarea_c'Identity)) loop
		delay 0.0;
	end loop;
	Abort_Task(tarea_e'Identity);
	Abort_Task(tarea1'Identity);
	Abort_Task(tarea2'Identity);
	delay 1.0;
	monitor.all.imprimir_msg("Fin del programa main ");

end main;
