with Ada.Text_IO; use Ada.Text_IO;

package body pkg_sem is

   procedure wait (sem : in out sem_t ; id: out Task_Id) is
      begin
         sem.wait(id);
      end wait;

   procedure signal (sem : in out sem_t ; id: in Task_Id) is
      begin
         sem.signal(id);
      end signal;

   protected body sem_t is
      entry wait (tarea: out Task_Id) when not (tarea_id = Null_Task_Id) is
      begin
         tarea := tarea_id;
         tarea_id := Null_Task_Id;

      end wait;

      procedure signal (tarea: in Task_Id) is
      begin
         tarea_id := tarea;
      end signal;

   end sem_t;
end pkg_sem;
