with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO.Unbounded_IO; use Ada.Text_IO.Unbounded_IO;

package body pkg_monitor is 

   procedure imprimir_msg(monitor : in out monitor_t; msg : in String) is
   begin
      monitor.bloquear;
      monitor.imprimir_msg(msg);
      monitor.liberar;
      delay(5.0);
   end imprimir_msg;

   procedure get_msg(monitor : in out monitor_t; msg_in : in String; msg_out : out Unbounded_String) is

   begin
   		monitor.bloquear;
   		select
            delay(10.0);
            msg_out:=To_Unbounded_String("-1");
            monitor.imprimir_aux("ERROR, Usuario despistado...");	
      then abort
         Put_Line(msg_in);
            msg_out := To_Unbounded_String(Get_Line);
   		end select;
   		monitor.liberar;
   end get_msg;

protected body  monitor_t is 

      entry imprimir_msg(msg      : in String) when estado = LIBRE is
         begin
         Put_Line(To_Unbounded_String(msg));
       end imprimir_msg;   

      entry imprimir_aux(msg  : in String)  when estado = LIBRE is
      begin
         estado := OCUPADO;
      		Put_Line(To_Unbounded_String(msg));
      		estado := LIBRE;
      end imprimir_aux;

      entry bloquear when estado = LIBRE is
      begin
         estado := OCUPADO;
      end bloquear;

      procedure liberar is
      begin
         estado := LIBRE;
      end liberar;

end monitor_t;


end pkg_monitor;
