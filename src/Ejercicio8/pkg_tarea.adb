with Ada.Text_IO; use Ada.Text_IO;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Task_Identification; use Ada.Task_Identification;

with pkg_monitor; use pkg_monitor;
with pkg_sem; use pkg_sem;
with pkg_funct_time; use pkg_funct_time;

package body pkg_tarea is
	task body tarea_periodica_t is
		Activacion: constant Time_Span:= To_Time_Span(1.0);
                Periodo: constant Time_Span := Milliseconds(per);
                Computo: constant Time_Span:= Milliseconds((per/2));
                Identificador : Task_Id := Current_Task ;
	begin
         delay To_Duration(Activacion);
         select
            delay To_Duration(Periodo);
            sem.all.signal(Identificador);
		delay To_Duration(Computo);
               then abort
            monitor.all.imprimir_aux("Soy la tarea: "&Image(Identificador)&" y son las: " &time_to_string );
            end select;

	end tarea_periodica_t;

   task body tarea_eventos_t is
      Identificador : Task_Id;
   begin
      loop
         sem.all.wait(Identificador);
         monitor.all.imprimir_aux("La tarea: "&Image(Identificador)&" no ha cumplido el plazo ");
         end loop;
	end tarea_eventos_t;


task body tarea_codigo_t is
		msg_aux : Unbounded_String;
	begin
		delay 5.0;
		loop
			msg_aux := To_Unbounded_String("");
			monitor.all.imprimir_msg("");
			get_msg(monitor.all, " Introduce el código:", msg_aux);
		if msg_aux = "ERROR" then
			monitor.all.imprimir_aux(" ERROR > LA ISLA VA A ESTALLAR!!!");
			monitor.all.imprimir_aux("");
			monitor.all.liberar;
			Abort_Task(Current_Task);
			exit;
		elsif msg_aux = "1234" then
			monitor.all.imprimir_aux(" CORRECTO!");
			monitor.all.imprimir_aux("");
		else
			monitor.all.imprimir_aux(" CODIGO ERRONEO > LA ISLA VA A ESTALLAR!!!");
			monitor.all.imprimir_aux("");
			monitor.all.liberar;
			Abort_Task(Current_Task);
			exit;
		end if;
		monitor.all.liberar;
		delay 10.0;
	end loop;
end tarea_codigo_t;

end pkg_tarea;
