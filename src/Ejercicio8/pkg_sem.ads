with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;
with Ada.Task_Identification; use Ada.Task_Identification;

package pkg_sem is

	protected type sem_t(valor_inicial : integer := 0) is
		entry wait(tarea: out Task_Id);
		procedure signal(tarea: in Task_Id);

	private
		state : boolean := false;
		tarea_id : Task_Id := Null_Task_Id;
	end sem_t;

	type p_sem is access sem_t;

end pkg_sem;
