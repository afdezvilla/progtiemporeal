with Ada.Text_IO; use Ada.Text_IO;
with pkg_monitor; use pkg_monitor;
with pkg_sem; use pkg_sem;
with pkg_funct_time; use pkg_funct_time;
with Ada.Task_Identification; use Ada.Task_Identification;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Calendar;

package pkg_tarea is

	type p_integer is access integer;
	task type tarea_periodica_t(monitor : p_monitor; sem : p_sem;per : integer);
	task type tarea_eventos_t(monitor : p_monitor; sem : p_sem);
	task type tarea_codigo_t(monitor : p_monitor);

end pkg_tarea;
