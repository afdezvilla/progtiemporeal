with Ada.Calendar;

package body pkg_funct_time is
	function time_to_string return string is
		use Ada.Calendar;
		reloj : Ada.Calendar.Time := clock;
		ano : Year_Number := Year_Number'First;
		mes : Month_Number := Month_Number'First;
		dia : Day_Number := Day_Number'First;
		segundos : Day_Duration := Day_Duration'First;
		hora : integer := 0;
		minutos : integer := 0;
		segundo : integer := 0;
	begin
		reloj := clock;
		split(reloj,ano,mes,dia,segundos);
		hora := abs(integer(segundos) / 3600);
		minutos := ((integer(segundos) - (hora * 3600)) / 60);
		segundo := integer(segundos) - hora*3600 - minutos*60;
		return ("["&hora'img & ":" & minutos'img & ":" & segundo'img & "]");
	end time_to_string;
end pkg_funct_time;
