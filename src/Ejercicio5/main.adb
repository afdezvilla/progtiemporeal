with Text_IO; use Text_IO;
with Ada.Real_Time; use Ada.Real_Time;

with pkg_tarea; use pkg_tarea;

procedure main is
   type ciclo is mod 4; 
   Marco : ciclo := 0;
   Siguienteciclo : Time := Clock;
   Semiperiodo : Time_span := Milliseconds(250);
begin
	Put_Line("---- Inicio del programa main ----");
   loop
      delay duration(1.0);
      case Marco is
         when 0 => A; B; D; Put_Line("---- Fin Marco 0 ----");
         when 1 => A; B; C; Put_Line("---- Fin Marco 1 ----");
         when 2 => A; B; C; E; Put_Line("---- Fin Marco 2 ----");
         when 3 => A; B; D; Put_Line("---- Fin Marco 3 ----");
      end case;
      Marco := Marco + 1;
      Siguienteciclo := Siguienteciclo + Semiperiodo;
      delay until Siguienteciclo;
   end loop;
   Put_Line("---- Fin del programa main ----");
end main;
