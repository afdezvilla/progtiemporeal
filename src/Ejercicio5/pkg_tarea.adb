package body pkg_tarea is 
	procedure A is
	begin
		delay 10.0;
		Put_Line("---- Fin Tarea A ----");	
	end A; 
	procedure B is 
	begin
		delay 8.0;
		Put_Line("---- Fin Tarea B ----");	
	end B;
	procedure C is 
	begin
		delay 5.0;
		Put_Line("---- Fin Tarea C ----");	
	end C;
	procedure D is 
	begin
		delay 4.0;
		Put_Line("---- Fin Tarea D ----");	
	end D;
	procedure E is 
	begin
		delay 2.0;
		Put_Line("---- Fin Tarea E ----");	
	end E;
end pkg_tarea;
