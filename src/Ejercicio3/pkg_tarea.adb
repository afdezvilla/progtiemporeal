with Ada.Text_IO; use Ada.Text_IO;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Task_Identification; use Ada.Task_Identification;

package body pkg_tarea is
	task body tarea_periodica_t is
		Activacion: constant Time_Span:= To_Time_Span(1.0);
                Periodo: constant Time_Span := Milliseconds(2000);
                Computo: constant Time_Span:= Milliseconds(1000);
                SiguienteLlamada: Time:= Clock;
                InicioEjecucion: Time := CLock;
		Variable: Integer:= 0;
	begin
		delay To_Duration(Activacion);
		SiguienteLlamada:= Clock + Periodo;
		Variable:= 0;
		InicioEjecucion:= Clock;
		while (Clock - InicioEjecucion) <= Computo loop
			Put_Line("Tarea("&Image(Current_Task)&"):Variable interna: "& Integer'Image(Variable));  
			Variable:= Variable + 1;	
      end loop;
        delay until (SiguienteLlamada);
        SiguienteLlamada:= SiguienteLlamada + Periodo;
	end tarea_periodica_t;
end pkg_tarea;
